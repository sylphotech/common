#pragma once

#include <string>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

namespace entity
{
    struct project
    {
        const char* path;
        const char* compile_cmd;

        project () { }
        project (
            const char* p,
            const char* ccmd
        )
        {
            path = p;
            compile_cmd = ccmd;
        }

        const char* serialize()
        {
            // TODO check if any of the values are null (and skip them)
            // create string to store json
            std::string json_string = "{";

            // populate values of json
            json_string += "\"path\":\"" + path + "\"";
            json_string += "\"compile_cmd\":\"" + compile_cmd + "\"";

            // terminate json
            json_string += "}";

            return json_string.c_str();
        }

        deserialize (const char* json_string)
        {
            // TODO check if JSON is uncorrupt
            // TODO only parse values that are present
            // parse json
            Document d;
            d.Parse(json_string);

            // construct obect from json
            path = d["path"].GetString().c_str();
            compile_cmd = d["compile_cmd"].GetString().c_str();
        }
    };
};
